
CREATE DATABASE elvis2;

CREATE TABLE mesa (
id INTEGER NOT NULL AUTO_INCREMENT,
estado VARCHAR (30) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE carta (
id INTEGER NOT NULL AUTO_INCREMENT,
nombre VARCHAR (30) NOT NULL,
descripcion VARCHAR (300) NOT NULL,
precio DOUBLE NOT NULL CHECK (precio>=0),
PRIMARY KEY (id)
);

CREATE TABLE pedido (
id INTEGER NOT NULL AUTO_INCREMENT,
mesa_id INTEGER (11),
pagado BOOLEAN,
tiempo INTEGER (11),
precio DOUBLE CHECK (precio>=0), 
CONSTRAINT pedido_PK PRIMARY KEY (id),
KEY FK_mesa (mesa_id),
CONSTRAINT `FK_mesa` FOREIGN KEY (mesa_id) REFERENCES mesa (id)
);

CREATE TABLE johnny (
nombre VARCHAR (30),
accion VARCHAR (500),
PRIMARY KEY (nombre)
);		

CREATE TABLE alimentos_pedido (
pedido_id INTEGER NOT NULL,
carta_id INTEGER NOT NULL,
cantidad INTEGER NOT NULL CHECK (cantidad>=0),
CONSTRAINT alimentos_pedido_PK PRIMARY KEY (pedido_id,carta_id),
FOREIGN KEY (pedido_id) REFERENCES pedido (id),
FOREIGN KEY (carta_id) REFERENCES carta (id)
);

/*Datos estáticos de la base de datos*/

INSERT INTO `elvis2`.`carta` (`id`, `nombre`, `descripcion`, `precio`) VALUES (NULL, 'Suspicious  Mind', 'maravillosa hamburguesa,¼ de libra, con 
cebolla a la plancha, bacon y queso.', '5.5');

INSERT INTO `elvis2`.`carta` (`id`, `nombre`, `descripcion`, `precio`) VALUES (NULL, 'My Way', 'hamburguesa de ¼ de libra con 
lechuga  y  tomate  e  ingredientes  a  escoger entre relish, bacon y queso.', '4');

INSERT INTO `elvis2`.`carta` (`id`, `nombre`, `descripcion`, `precio`) VALUES (NULL, 'New  York  cheese  cake', 'la  original  a  la  vuelta  de  la  esquina', '4.95');

INSERT INTO `elvis2`.`carta` (`id`, `nombre`, `descripcion`, `precio`) VALUES (NULL, 'Refresco', 'Coca-Cola o Seven-Up', '2');

INSERT INTO `elvis2`.`carta` (`id`, `nombre`, `descripcion`, `precio`) VALUES (NULL, 'Chesse Fries', 'Patatas con queso cheddar y bacon', '3');

INSERT INTO `elvis2`.`carta` (`id`, `nombre`, `descripcion`, `precio`) VALUES (NULL, 'Ingrediente Extra', 'A elegir entre relish  (salsa  de 
pepinillo dulce picado), bacon y queso cheddar', '0.5');

INSERT INTO `elvis2`.`carta` (`id`, `nombre`, `descripcion`, `precio`) VALUES (NULL, 'Refresco Refill', 'Coca-Cola o Seven-Up con Refill', '5');

/*Datos estáticos de la base de datos*/

/*Datos iniciales de las mesas */

INSERT INTO `elvis2`.`mesa` (`id`, `estado`) VALUES ('1', 'Libre');

INSERT INTO `elvis2`.`mesa` (`id`, `estado`) VALUES ('2', 'Libre');

INSERT INTO `elvis2`.`mesa` (`id`, `estado`) VALUES ('3', 'Libre');

INSERT INTO `elvis2`.`mesa` (`id`, `estado`) VALUES ('4', 'Libre');

/*Datos iniciales de las mesas */

/* Estado inicial de Johnny */

INSERT INTO `elvis2`.`johnny` (`nombre`, `accion`) VALUES ('Johnny', 'Johnny descansa');

/* Estado inicial de Johnny */