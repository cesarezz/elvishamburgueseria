package elvisHamburgueseria;


import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.springmvc.elvisHamburgueseria.dao.JohnnyDAO;
import com.springmvc.elvisHamburgueseria.models.Johnny;
import com.springmvc.elvisHamburgueseria.service.JohnnyService;

public class JohnnyServiceTests {
	
	@Mock
	private Johnny johnny;
	
	@Mock
	private JohnnyDAO johnnyDAO;
	
	@Mock
	private JohnnyService johnnyService;
	
	@Captor
    ArgumentCaptor<Johnny> captor;
	
	@Before
    public void setupMock() {
    	
    	//Inicializa la creacion de las clases con anotaciones @Mock
    	MockitoAnnotations.initMocks(this);
    	
    }
	
	@Test
    public void testGetEstado(){
		
		 Johnny johnny = null;
		
		 //Simulamos la respuesta de un metodo.
		 when(johnnyDAO.getEstado()).thenReturn(johnny);
	     assertEquals(johnny,johnnyDAO.getEstado());
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
	     johnnyService.getEstado();
	     verify(johnnyDAO).getEstado();
	     
	}

	@Test
	public void TestInicializaJohnny(){
		
		 //Comprobamos que update se ejecute correctamente
		 Johnny johnny = new Johnny();
		 johnny.setNombre("Johnny");		 
		 doNothing().when(johnnyDAO).update(johnny);	
		 johnnyDAO.update(johnny);	
		 verify(johnnyDAO,times(1)).update(captor.capture());
		 assertEquals(captor.getValue(), johnny);	
		 
		 //Simulamos la respuesta de un metodo.
		 when(johnnyDAO.selectByNombre("Johnny")).thenReturn(johnny);
		 
		 //Validamos la respuesta		 
	     assertEquals(johnny,johnnyDAO.selectByNombre("Johnny"));
	     
	     johnnyService.inicializaJohnny();
	     
	    //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
	     verify(johnnyDAO).selectByNombre("Johnny");
	     
	     //Comprobamos que sólo se llama una vez a selectByNombre
	     verify(johnnyDAO, atLeast(1)).selectByNombre("Johnny");
	     verify(johnnyDAO, atMost(1)).selectByNombre("Johnny");
		
	}
}
