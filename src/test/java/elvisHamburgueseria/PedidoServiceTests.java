package elvisHamburgueseria;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.springmvc.elvisHamburgueseria.dao.PedidoDAO;
import com.springmvc.elvisHamburgueseria.models.Pedido;
import com.springmvc.elvisHamburgueseria.service.PedidoService;

public class PedidoServiceTests {
	
	@Mock
	private Pedido pedido;
	
	@Mock
	private PedidoDAO pedidoDAO;
	
	@Mock
	private PedidoService pedidoService;
	
	@Captor
    ArgumentCaptor<Pedido> captor;
	
	@Before
    public void setupMock() {
    	
    	//Inicializa la creacion de las clases con anotaciones @Mock
    	MockitoAnnotations.initMocks(this);
    	
    }
	
	@Test
    public void TestInsertMesa(){
			
		 Integer cinco = new Integer(5);
		
		 Integer dos = new Integer(2);
		
		 //Simulamos la respuesta de un metodo.
		 when(pedidoDAO.insertMesa(cinco,dos)).thenReturn(2);
	     assertEquals(dos,pedidoDAO.insertMesa(cinco,dos));
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
	     pedidoService.insertMesa("5", "2");
	     verify(pedidoDAO).insertMesa(cinco, dos);			
		
	}
	
	@Test
    public void TestInsert(){
		
		 Pedido pedido = new Pedido();
		 pedido.setPagado(false);
		 
		 //Simulamos la respuesta de un metodo.
		 when(pedidoDAO.insert(pedido)).thenReturn(pedido);
		 assertEquals(pedido,pedidoDAO.insert(pedido));
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
	     pedidoService.insert();
	     verify(pedidoDAO).insert(pedido);		
		
	}
	
	@Test
    public void TestCambiarPagado(){
		
		//Comprobamos que update se ejecute correctamente
		Pedido pedido = new Pedido();
		pedido.setPagado(false);
		pedido.setId(5);
		
		doNothing().when(pedidoDAO).update(pedido);				
		pedidoDAO.update(pedido);			
		verify(pedidoDAO,times(1)).update(captor.capture());			
		assertEquals(captor.getValue(), pedido);	
		
		//Comprobamos que se ejecuta update llamando a cambiarPagado del servicio
		pedidoService.cambiarPagado(5);
		verify(pedidoDAO,times(1)).update(captor.capture());			
		
	}
	
	@Test
    public void TestPedidoPorId(){
		
		 //Comprobamos que update se ejecute correctamente
		 Pedido pedido1 = new Pedido();
		 pedido.setPagado(false);
		 pedido.setId(1);
		
		 doNothing().when(pedidoDAO).update(pedido1);	
		 pedidoDAO.update(pedido1);	
		 verify(pedidoDAO,times(1)).update(captor.capture());
		 assertEquals(captor.getValue(), pedido1);	
		 
		 //Simulamos la respuesta de un metodo.
		 when(pedidoDAO.selectById(1)).thenReturn(pedido);
		 
		 //Validamos la respuesta		 
	     assertEquals(pedido,pedidoDAO.selectById(1));
	     
	     pedidoService.pedidoPorId(1);
	     
	     //Comprobamos que el metodo del servicio ejecuta el metodo correcto del DAO. 
	     verify(pedidoDAO).selectById(1);
	     
	     //Comprobamos que sólo se llama una vez a selectById con la mesa seleccionada
	     verify(pedidoDAO, atLeast(1)).selectById(1);
	     verify(pedidoDAO, atMost(1)).selectById(1);
		
	}
	
	@Test
    public void TestInsertTiempoPrecio(){
		
		//Comprobamos que insertTiempo se ejecute correctamente
		Pedido pedido = new Pedido();
		pedido.setPagado(false);
		pedido.setId(1);
		
		Integer uno = new Integer(1);
		
		//Simulamos la respuesta de un método.
		when(pedidoDAO.insertTiempoPrecio(1, 500, 10.5)).thenReturn(uno);		
		assertEquals(uno,pedidoDAO.insertTiempoPrecio(1, 500, 10.5));
		
		//Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
	     pedidoService.insertTiempoPrecio(1, 500, 10.5);
	     verify(pedidoDAO).insertTiempoPrecio(1, 500, 10.5);	
	     
	}
	
	@Test
    public void TestTodosPedidos(){
		
		 Pedido pedido = new Pedido();
		 pedido.setPagado(false);
		 pedido.setId(1);
		
		 List<Pedido> pedidos = null;
		 
		 //Simulamos la respuesta de un metodo.
		 when(pedidoDAO.getPedidos()).thenReturn(pedidos);
		 assertEquals(pedidos,pedidoDAO.getPedidos());
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
	     pedidoService.todosPedidos();
	     verify(pedidoDAO).getPedidos();
		
	}
	
	@Test
    public void TestDelete(){
		
		 Pedido pedido = new Pedido();
		 pedido.setPagado(false);
		 pedido.setId(1);
		
		 Integer uno = new Integer(1);
		 
		 //Simulamos la respuesta de un metodo.
		 when(pedidoDAO.delete(pedido)).thenReturn(uno);
		 assertEquals(uno,pedidoDAO.delete(pedido));
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
	     pedidoService.delete(pedido);
	     verify(pedidoDAO).delete(pedido);
		
	}

}
