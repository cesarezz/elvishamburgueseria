package elvisHamburgueseria;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.springmvc.elvisHamburgueseria.dao.AlimentosPedidoDAO;
import com.springmvc.elvisHamburgueseria.models.Alimentos_Pedido;
import com.springmvc.elvisHamburgueseria.models.Alimentos_PedidoId;
import com.springmvc.elvisHamburgueseria.service.AlimentosPedidoService;

public class AlimentosPedidoServiceTests {
	
	@Mock
	private Alimentos_Pedido alimentos_pedido;
	
	@Mock
	private Alimentos_PedidoId alimentos_pedidoId;
	
	@Mock
	private AlimentosPedidoDAO alimentosPedidoDAO;
	
	@Mock
	private AlimentosPedidoService alimentosPedidoService;
	
	@Before
    public void setupMock() {
    	
    	//Inicializa la creacion de las clases con anotaciones @Mock
    	MockitoAnnotations.initMocks(this);
    	
    }
	
	@Test
    public void testInsert(){
		
		 Integer uno = new Integer(1);
		
		 Alimentos_PedidoId alimentos_pedidoId = new Alimentos_PedidoId(uno,uno);
		 
		 //Simulamos la respuesta de un metodo.
		 when(alimentosPedidoDAO.insert(alimentos_pedido)).thenReturn(alimentos_pedidoId);
		 assertEquals(alimentos_pedidoId,alimentosPedidoDAO.insert(alimentos_pedido));
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
		 alimentosPedidoService.insert(alimentos_pedido);
	     verify(alimentosPedidoDAO).insert(alimentos_pedido);		
		
	}
	
	@Test
    public void testSelectByPedidoCarta(){
		
		 Integer uno = new Integer(1);
		 Integer cinco = new Integer(5);
		
		 Alimentos_Pedido alimentos_pedido = new Alimentos_Pedido();
		 alimentos_pedido.setPedido_id(uno);
		 alimentos_pedido.setCarta_id(cinco);
		 alimentos_pedido.setCantidad(cinco);
		 //Simulamos la respuesta de un metodo.
		 when(alimentosPedidoDAO.selectByPedidoCarta(uno, cinco)).thenReturn(alimentos_pedido);
		 assertEquals(alimentos_pedido,alimentosPedidoDAO.selectByPedidoCarta(uno, cinco));
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
		 alimentosPedidoService.selectByPedidoCarta(uno, cinco);
	     verify(alimentosPedidoDAO).selectByPedidoCarta(uno, cinco);
		
	}
	
	@Test
    public void testSelectByPedido(){
		
		 Integer uno = new Integer(1);
		
		 List<Alimentos_Pedido> pedidos = new ArrayList<Alimentos_Pedido>();
		 pedidos.add(alimentos_pedido);
		 pedidos.add(alimentos_pedido);
		 
		 
		 
		 //Simulamos la respuesta de un metodo.
		 when(alimentosPedidoDAO.selectByPedido(uno)).thenReturn(pedidos);
		 assertEquals(pedidos,alimentosPedidoDAO.selectByPedido(uno));
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
		 alimentosPedidoService.selectByPedido(uno);
	     verify(alimentosPedidoDAO).selectByPedido(uno);
		
	}
	
	@Test
    public void testDelete(){
		
		 Integer uno = new Integer(1); 
		 
		 //Simulamos la respuesta de un metodo.
		 when(alimentosPedidoDAO.delete(uno)).thenReturn(uno);
		 assertEquals(uno,alimentosPedidoDAO.delete(uno));
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
		 alimentosPedidoService.delete(uno);
	     verify(alimentosPedidoDAO).delete(uno);
		
	}
	
	@Test
    public void testGetAll(){

		
		 List<Alimentos_Pedido> pedidos = new ArrayList<Alimentos_Pedido>();
		 pedidos.add(alimentos_pedido);
		 pedidos.add(alimentos_pedido);
		 
		 //Simulamos la respuesta de un metodo.
		 when(alimentosPedidoDAO.getAll()).thenReturn(pedidos);
		 assertEquals(pedidos,alimentosPedidoDAO.getAll());
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
		 alimentosPedidoService.getAll();
	     verify(alimentosPedidoDAO).getAll();
		
	}
	
	@Test
    public void testDeleteAlimentos(){
		
		Integer uno = new Integer(1);
		
		Alimentos_PedidoId alimentos_pedidoId = new Alimentos_PedidoId(uno,uno);
		
		//Simulamos la respuesta de un metodo.
		 when(alimentosPedidoDAO.deleteAlimentos(alimentos_pedido)).thenReturn(alimentos_pedidoId);
		 assertEquals(alimentos_pedidoId,alimentosPedidoDAO.deleteAlimentos(alimentos_pedido));
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
		 alimentosPedidoService.deleteAlimentos(alimentos_pedido);
	     verify(alimentosPedidoDAO).deleteAlimentos(alimentos_pedido);
	}
	
	

}
