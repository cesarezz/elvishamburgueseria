package elvisHamburgueseria;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.springmvc.elvisHamburgueseria.dao.MesaDAO;
import com.springmvc.elvisHamburgueseria.models.Mesa;
import com.springmvc.elvisHamburgueseria.service.MesaService;

public class MesaServiceTests {
	
		@Mock
		private Mesa mesa;
		
		@Mock
		private MesaDAO mesaDAO;
		
		@Mock
		private MesaService mesaService;
		
		@Captor
	    ArgumentCaptor<Mesa> captor;
		
		@Before
	    public void setupMock() {
	    	
	    	//Inicializa la creacion de las clases con anotaciones @Mock
	    	MockitoAnnotations.initMocks(this);
	    	
	    }
		
		@Test
	    public void TestInicializarMesas(){
			
			 
			 //Comprobamos que update se ejecute correctamente
			 doNothing().when(mesaDAO).update(mesa);	
			 mesaDAO.update(mesa);	
			 verify(mesaDAO,times(1)).update(captor.capture());
			 assertEquals(captor.getValue(), mesa);	
			 
			 //Simulamos la respuesta de un metodo.
			 when(mesaDAO.selectById(1)).thenReturn(mesa);
			 when(mesaDAO.selectById(2)).thenReturn(mesa);	
			 when(mesaDAO.selectById(3)).thenReturn(mesa);
			 when(mesaDAO.selectById(4)).thenReturn(mesa);		
			 
			 
			 //Validamos la respuesta		 
		     assertEquals(mesa,mesaDAO.selectById(1));
		     assertEquals(mesa,mesaDAO.selectById(2));
		     assertEquals(mesa,mesaDAO.selectById(3));
		     assertEquals(mesa,mesaDAO.selectById(4));
		     
		     mesaService.inicializarMesas();
		     
		     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
		     verify(mesaDAO).selectById(1);

		     verify(mesaDAO).selectById(2);

		     verify(mesaDAO).selectById(3);

		     verify(mesaDAO).selectById(4);
		     
		     //Comprobamos que sólo se llama una vez a selectById por cada mesa
		     verify(mesaDAO, atLeast(1)).selectById(1);
		     verify(mesaDAO, atMost(1)).selectById(1);
		     
		     verify(mesaDAO, atLeast(1)).selectById(2);
		     verify(mesaDAO, atMost(1)).selectById(2);
		     
		     verify(mesaDAO, atLeast(1)).selectById(3);
		     verify(mesaDAO, atMost(1)).selectById(3);
		     
		     verify(mesaDAO, atLeast(1)).selectById(4);
		     verify(mesaDAO, atMost(1)).selectById(4);
			
		}
		
		@Test
	    public void TestCambiaEstado(){
			
			//Comprobamos que update se ejecute correctamente
			doNothing().when(mesaDAO).update(mesa);				
			mesaDAO.update(mesa);			
			verify(mesaDAO,times(1)).update(captor.capture());			
			assertEquals(captor.getValue(), mesa);	
			
			//Comprobamos que se ejecuta update llamando a cambiaEstado del servicio
			mesaService.cambiaEstado(mesa);
			verify(mesaDAO,times(1)).update(captor.capture());			
			
		}
		
		
		@Test
	    public void TestShowMesaById(){
			
			 //Comprobamos que update se ejecute correctamente
			 doNothing().when(mesaDAO).update(mesa);	
			 mesaDAO.update(mesa);	
			 verify(mesaDAO,times(1)).update(captor.capture());
			 assertEquals(captor.getValue(), mesa);	
			 
			 //Simulamos la respuesta de un metodo.
			 when(mesaDAO.selectById(1)).thenReturn(mesa);
			 
			 //Validamos la respuesta		 
		     assertEquals(mesa,mesaDAO.selectById(1));
		     
		     mesaService.showMesaById(1);
		     
		     //Comprobamos que el metodo del servicio ejecuta el metodo correcto del DAO. 
		     verify(mesaDAO).selectById(1);
		     
		     //Comprobamos que sólo se llama una vez a selectById con la mesa seleccionada
		     verify(mesaDAO, atLeast(1)).selectById(1);
		     verify(mesaDAO, atMost(1)).selectById(1);
		}
		

	}
