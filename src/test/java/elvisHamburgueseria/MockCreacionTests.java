package elvisHamburgueseria;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.springmvc.elvisHamburgueseria.dao.*;
import com.springmvc.elvisHamburgueseria.models.*;

public class MockCreacionTests {
	
	private CartaDAO cartaDAO;
	private MesaDAO mesaDAO;
	private PedidoDAO pedidoDAO;
	
	@Mock
	private AlimentosPedidoDAO alimentosPedidoDAO;
	@Mock
	private JohnnyDAO johnnyDAO;
	
    private Carta carta;
    private Mesa mesa;
    private Pedido pedido;
    
    @Mock
    private Alimentos_Pedido alimentosPedido;
    @Mock
    private Johnny johnny;
    
    @Before
    public void setupMock() {
    	
    	//Inicializa la creacion de las clases con anotaciones @Mock
    	MockitoAnnotations.initMocks(this);
    	
    	carta = mock(Carta.class);
    	cartaDAO = mock(CartaDAO.class);
    	
    	mesa = mock(Mesa.class);
    	mesaDAO = mock(MesaDAO.class);
    	
    	pedido = mock(Pedido.class);
    	pedidoDAO = mock(PedidoDAO.class);
    	
    }
    
    @Test
    public void testMockCreation(){
    	
        assertNotNull(carta);
        assertNotNull(cartaDAO);
        
        assertNotNull(mesa);
        assertNotNull(mesaDAO);
        
        assertNotNull(pedido);
        assertNotNull(pedidoDAO);
    }

}
