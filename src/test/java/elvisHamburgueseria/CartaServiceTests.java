package elvisHamburgueseria;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.springmvc.elvisHamburgueseria.dao.CartaDAO;
import com.springmvc.elvisHamburgueseria.models.Carta;
import com.springmvc.elvisHamburgueseria.service.CartaService;

public class CartaServiceTests {
	
	@Mock
	private Carta Carta;
	
	@Mock
	private CartaDAO cartaDAO;
	
	@Mock
	private CartaService cartaService;
	
	@Before
    public void setupMock() {
    	
    	//Inicializa la creacion de las clases con anotaciones @Mock
    	MockitoAnnotations.initMocks(this);
    	
    }
	
	@Test
    public void testShowAlimentos(){
		
		 List<Carta> cartas = null;
		
		 //Simulamos la respuesta de un metodo.
		 when(cartaDAO.selectAll()).thenReturn(cartas);
	     assertEquals(cartas,cartaDAO.selectAll());
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
	     cartaService.showAlimentos();
	     verify(cartaDAO).selectAll();
		
	}
	
	@Test
    public void testShowAlimentoById(){
		
		 Carta carta = new Carta();
		 
		 Integer i = new Integer(5);
		
		 //Simulamos la respuesta de un metodo.
		 when(cartaDAO.selectById(i)).thenReturn(carta);
	     assertEquals(carta,cartaDAO.selectById(i));
	     
	     //Comprobamos que el metodo del servicio ejecuta los metodos del DAO correctos. 
	     cartaService.showAlimentoById(i);
	     verify(cartaDAO).selectById(5);
		
	}
	

}
