package com.springmvc.elvisHamburgueseria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Entity
public class Carta {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	@Column(name="id", nullable=false)
	private Integer id;
	

	@Size(min = 1, max = 30, message = "Nombre maximo 30 caracteres")
	@Column(name="nombre", nullable=false)
	private String nombre;
	

	@Size(min = 1, max = 300, message = "Nombre maximo 300 caracteres")
	@Column(name="descripcion", nullable=false)
	private String descripcion;
	

	@Min(0)
	@Column(name="precio", nullable=false)
	private Double precio;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
}
