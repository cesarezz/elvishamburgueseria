package com.springmvc.elvisHamburgueseria.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

@Entity
@Table(name="mesa")
public class Mesa {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	@Column(name="id", nullable=false)
	private Integer id;
	
	@Size(min = 1, max = 30, message = "Estado maximo 30 caracteres")
	@Column(name="estado", nullable=false)
	private String estado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Mesa [id=" + id + ", estado=" + estado + "]";
	}

}
