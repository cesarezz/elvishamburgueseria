package com.springmvc.elvisHamburgueseria.models;

import java.io.Serializable;

public class Alimentos_PedidoId implements Serializable {
	 
		private Integer pedido_id;
		private Integer carta_id;
	 
		public Alimentos_PedidoId() {
	 
		}
	 
		public Alimentos_PedidoId(Integer pedido_id, Integer carta_id) {
			this.pedido_id = pedido_id;
			this.carta_id = carta_id;
		}

		public Integer getPedido_id() {
			return pedido_id;
		}

		public Integer getCarta_id() {
			return carta_id;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((carta_id == null) ? 0 : carta_id.hashCode());
			result = prime * result
					+ ((pedido_id == null) ? 0 : pedido_id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Alimentos_PedidoId other = (Alimentos_PedidoId) obj;
			if (carta_id == null) {
				if (other.carta_id != null)
					return false;
			} else if (!carta_id.equals(other.carta_id))
				return false;
			if (pedido_id == null) {
				if (other.pedido_id != null)
					return false;
			} else if (!pedido_id.equals(other.pedido_id))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "Alimentos_PedidoId [pedido_id=" + pedido_id + ", carta_id="
					+ carta_id + "]";
		}
	
}
