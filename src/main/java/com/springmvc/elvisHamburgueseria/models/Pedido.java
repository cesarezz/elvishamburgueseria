package com.springmvc.elvisHamburgueseria.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Pedido {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	@Column(name="id", nullable=false)
	private Integer id;

	@ManyToOne
	@JoinColumn(name="mesa_id")
	private Mesa mesa;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "pedido_id", nullable=true)
	private Set<Alimentos_Pedido> alimentos_pedido = new HashSet<Alimentos_Pedido>();
	
	@Column(name="pagado", nullable=true)
	private Boolean pagado;
	
	@Column(name="tiempo", nullable=true)
	private Integer tiempo;
	
	@Column(name="precio", nullable=true)
	private Double precio;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}	

	public Boolean getPagado() {
		return pagado;
	}

	public void setPagado(Boolean pagado) {
		this.pagado = pagado;
	}
	
	public Integer getTiempo() {
		return tiempo;
	}

	public void setTiempo(Integer tiempo) {
		this.tiempo = tiempo;
	}
	
	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	public Set<Alimentos_Pedido> getAlimentos_pedido() {
		return alimentos_pedido;
	}

	public void setAlimentos_pedido(Set<Alimentos_Pedido> alimentos_pedido) {
		this.alimentos_pedido = alimentos_pedido;
	}

	@Override
	public String toString() {
		return "Pedido [id=" + id + ", mesa=" + mesa + ", pagado=" + pagado
				+ "]";
	}

}
