package com.springmvc.elvisHamburgueseria.models;

import java.util.List;


public class AlimentosPedidoWrapper {

    private List<Alimentos_Pedido> alimentosPedidos;

    /**
     * @return the persons
     */
    public List<Alimentos_Pedido> getAlimentosPedidos() {
        return alimentosPedidos;
    }

    /**
     * @param persons the persons to set
     */
    public void setAlimentosPedidos(List<Alimentos_Pedido> alimentosPedidos) {
        this.alimentosPedidos = alimentosPedidos;
    }

	@Override
	public String toString() {
		
		String response = null;
		
		for (Alimentos_Pedido ap: alimentosPedidos){
			response = response + "/n" + ap.toString();
		  }
		return response;
	}
    
    
}
	

