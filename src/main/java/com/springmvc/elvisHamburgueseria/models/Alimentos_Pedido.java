package com.springmvc.elvisHamburgueseria.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;


/*@Embeddable
class CompuestaPK implements Serializable{ 
	
    @Column
    private Integer pedido_id;
    
    @Column
    private Integer carta_id;
    
	public Integer getPedido_id() {
		return pedido_id;
	}
	public void setPedido_id(Integer pedido_id) {
		this.pedido_id = pedido_id;
	}
	public Integer getCarta_id() {
		return carta_id;
	}
	public void setCarta_id(Integer carta_id) {
		this.carta_id = carta_id;
	}
    
}*/

@Entity
@IdClass(Alimentos_PedidoId.class)
@Table(name="alimentos_pedido") 	
public class Alimentos_Pedido {
	
	/*@EmbeddedId
	private CompuestaPK alimentos_pedido_PK;*/
	@Id
    private Integer pedido_id;
    
	@Id
    private Integer carta_id;
	 
	@Min(0)
	@Column(name="cantidad", nullable=false)
	private Integer cantidad;

	public Integer getPedido_id() {
		return pedido_id;
	}

	public void setPedido_id(Integer pedido_id) {
		this.pedido_id = pedido_id;
	}

	public Integer getCarta_id() {
		return carta_id;
	}

	public void setCarta_id(Integer carta_id) {
		this.carta_id = carta_id;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
}
