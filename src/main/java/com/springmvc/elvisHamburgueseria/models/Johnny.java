package com.springmvc.elvisHamburgueseria.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Johnny {
	
	@Id 
	@Column(name="nombre", nullable=false)
	private String nombre;
	
	@Column(name="accion", nullable=false)
	private String accion;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

}
