package com.springmvc.elvisHamburgueseria.models;

public class MessageJohnny {
		 
	private String text;
	
    public MessageJohnny() {
    }
 
    public MessageJohnny(String text) {
	   this.text = text;
    }
 
    public String getText() {
        return text;
    }	    

}
