package com.springmvc.elvisHamburgueseria.service;

import com.springmvc.elvisHamburgueseria.models.Mesa;

public interface MesaService {

	public void inicializarMesas();
	public void cambiaEstado (Mesa mesa);
	public Mesa showMesaById(Integer id);
	
}
