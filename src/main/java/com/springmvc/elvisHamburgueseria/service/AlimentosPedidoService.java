package com.springmvc.elvisHamburgueseria.service;

import java.util.ArrayList;
import java.util.List;

import com.springmvc.elvisHamburgueseria.models.Alimentos_Pedido;
import com.springmvc.elvisHamburgueseria.models.Alimentos_PedidoId;

public interface AlimentosPedidoService {
	
	public Alimentos_PedidoId insert(Alimentos_Pedido alimentos_pedido);
	
	public Alimentos_Pedido selectByPedidoCarta(Integer idPedido, Integer idCarta);
	
	public List<Alimentos_Pedido> selectByPedido(Integer id);
	
	public Integer delete(Integer idPedido);
	
	public List<Alimentos_Pedido> getAll();
	
	public Alimentos_PedidoId deleteAlimentos(Alimentos_Pedido alimentos_pedido);
	
}
