package com.springmvc.elvisHamburgueseria.service;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.springmvc.elvisHamburgueseria.models.Carta;

public interface CartaService {
	
	public List<Carta> showAlimentos();
	public Carta showAlimentoById(Integer id);

}
