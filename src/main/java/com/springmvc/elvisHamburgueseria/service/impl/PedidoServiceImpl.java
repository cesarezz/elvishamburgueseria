package com.springmvc.elvisHamburgueseria.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springmvc.elvisHamburgueseria.dao.PedidoDAO;
import com.springmvc.elvisHamburgueseria.models.Mesa;
import com.springmvc.elvisHamburgueseria.models.Pedido;
import com.springmvc.elvisHamburgueseria.service.PedidoService;

@Service
public class PedidoServiceImpl implements PedidoService {
	
	@Autowired
    private PedidoDAO pedidoDAO;

	public void cambiarPagado(Integer id) {
		
		Pedido pedido = pedidoDAO.selectById(id);
		  
		pedido.setPagado(true);
		  
		pedidoDAO.update(pedido);
		
	}

	public Pedido pedidoPorId(Integer id) {
		 return (Pedido) pedidoDAO.selectById(id);
	}


	public Pedido insert() {
		
		Pedido pedido = new Pedido();
		pedido.setPagado(false);
		
		return pedidoDAO.insert(pedido);
	}

	@Override
	public Integer insertTiempoPrecio(Integer id, Integer tiempo, Double precio) {
		
		return pedidoDAO.insertTiempoPrecio(id,tiempo, precio);
		
	}

	@Override
	public List<Pedido> todosPedidos() {
		
		return pedidoDAO.getPedidos();
	}

	@Override
	public Integer insertMesa(String idPedido, String idMesa) {

		Integer idPedidoInt= new Integer(idPedido);
		
		Integer idMesaInt= new Integer(idMesa);
	
		return pedidoDAO.insertMesa(idPedidoInt,idMesaInt);		
		
	}

	@Override
	public Integer delete(Pedido pedido) {

		return pedidoDAO.delete(pedido);	
		
	}
}
