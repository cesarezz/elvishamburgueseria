package com.springmvc.elvisHamburgueseria.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import com.springmvc.elvisHamburgueseria.dao.JohnnyDAO;
import com.springmvc.elvisHamburgueseria.models.Johnny;
import com.springmvc.elvisHamburgueseria.service.JohnnyService;
import com.springmvc.elvisHamburgueseria.service.events.MesaEvent;

@Service
public class JohnnyServiceImpl implements JohnnyService, ApplicationListener<MesaEvent>  {
	
	@Autowired
    private JohnnyDAO johnnyDAO;
	
	public void onApplicationEvent(MesaEvent event) {
		
		String estado = event.getMesa().getEstado();
		Integer idMesa = event.getMesa().getId();
		Johnny johnny = johnnyDAO.selectByNombre("Johnny");
		
		if (estado.equalsIgnoreCase("EstadoLibre")){
			johnny.setAccion("Nuevos comensales en la mesa. Johnny va a tomar nota a la mesa" + idMesa.toString());
		} else if (estado.equalsIgnoreCase("EstadoEsperandoCuenta")){
			johnny.setAccion("Johnny lleva la cuenta y limpia la mesa " + idMesa.toString());
		} else {
			johnny.setAccion("Johnny descansa");
		}
		
		johnnyDAO.update(johnny);
		
	}
	
	public void inicializaJohnny() {
		
		Johnny johnny = johnnyDAO.selectByNombre("Johnny");
		
		johnny.setAccion("Johnny descansa");
		
		johnnyDAO.update(johnny);
		
	}

	@Override
	public Johnny getEstado() {		
		return johnnyDAO.getEstado();
	}

	

}
