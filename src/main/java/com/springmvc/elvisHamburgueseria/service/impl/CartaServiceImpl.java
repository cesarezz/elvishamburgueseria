package com.springmvc.elvisHamburgueseria.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springmvc.elvisHamburgueseria.dao.CartaDAO;
import com.springmvc.elvisHamburgueseria.models.Carta;
import com.springmvc.elvisHamburgueseria.service.CartaService;

@Service
public class CartaServiceImpl implements CartaService {
	
	private static final Logger logger = Logger.getLogger(CartaServiceImpl.class);

	@Autowired
    private CartaDAO cartaDAO;
	
	public List<Carta> showAlimentos() {
		
		return cartaDAO.selectAll();
	}

	public Carta showAlimentoById(Integer id) {
		
		return (Carta) cartaDAO.selectById(id);
				
	}

}
