package com.springmvc.elvisHamburgueseria.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

import com.springmvc.elvisHamburgueseria.dao.MesaDAO;
import com.springmvc.elvisHamburgueseria.models.Mesa;
import com.springmvc.elvisHamburgueseria.service.MesaService;
import com.springmvc.elvisHamburgueseria.service.events.MesaEvent;

@Service
public class MesaServiceImpl implements MesaService, ApplicationEventPublisherAware {
	
	
	private static final Logger logger = Logger.getLogger(MesaServiceImpl.class);
    
    @Autowired
    private MesaDAO mesaDAO;
    
    private ApplicationEventPublisher publisher;
    
	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
    	this.publisher = publisher;
	}
    

	public void inicializarMesas() {		
    	Mesa m = mesaDAO.selectById(1);
    	m.setEstado("Libre");	    	
    	mesaDAO.update(m);
    	
    	m = mesaDAO.selectById(2);
    	m.setEstado("Libre");	
    	mesaDAO.update(m);
    	
    	m = mesaDAO.selectById(3);
    	m.setEstado("Libre");	
    	mesaDAO.update(m);
    	
    	m = mesaDAO.selectById(4);
    	m.setEstado("Libre");
    	mesaDAO.update(m);
	}

	public void cambiaEstado(Mesa mesa) {
		mesaDAO.update(mesa);		
		publisher.publishEvent(new MesaEvent(this, "Nuevo Estado", mesa));
	}

	public Mesa showMesaById(Integer id) {
		
		return mesaDAO.selectById(id);
		
	}
	
}
