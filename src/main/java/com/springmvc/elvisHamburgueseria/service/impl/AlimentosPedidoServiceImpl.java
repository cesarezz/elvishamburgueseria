package com.springmvc.elvisHamburgueseria.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springmvc.elvisHamburgueseria.dao.AlimentosPedidoDAO;
import com.springmvc.elvisHamburgueseria.models.Alimentos_Pedido;
import com.springmvc.elvisHamburgueseria.models.Alimentos_PedidoId;
import com.springmvc.elvisHamburgueseria.service.AlimentosPedidoService;

@Service
public class AlimentosPedidoServiceImpl implements AlimentosPedidoService {
	
	@Autowired
    private AlimentosPedidoDAO alimentosPedidoDAO;

	@Override
	public Alimentos_PedidoId insert(Alimentos_Pedido alimentos_Pedido) {
		
		return alimentosPedidoDAO.insert(alimentos_Pedido);
		
	}

	@Override
	public Alimentos_Pedido selectByPedidoCarta(Integer idPedido,
			Integer idCarta) {
		
		return alimentosPedidoDAO.selectByPedidoCarta(idPedido, idCarta);
		
	}

	@Override
	public List<Alimentos_Pedido> selectByPedido(Integer id) {
		
		return alimentosPedidoDAO.selectByPedido(id);
	}

	@Override
	public Integer delete(Integer idPedido) {
		
		return alimentosPedidoDAO.delete(idPedido);
		
	}

	@Override
	public List<Alimentos_Pedido> getAll() {
		
		return alimentosPedidoDAO.getAll();
		
	}

	@Override
	public Alimentos_PedidoId deleteAlimentos(Alimentos_Pedido alimentos_pedido) {

		return alimentosPedidoDAO.deleteAlimentos(alimentos_pedido);
		
	}

}
