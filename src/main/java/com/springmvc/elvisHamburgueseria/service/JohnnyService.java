package com.springmvc.elvisHamburgueseria.service;

import com.springmvc.elvisHamburgueseria.models.Johnny;

public interface JohnnyService {
	
	public Johnny getEstado();
	
	public void inicializaJohnny();

}
