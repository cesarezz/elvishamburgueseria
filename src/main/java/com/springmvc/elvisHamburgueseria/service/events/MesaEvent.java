package com.springmvc.elvisHamburgueseria.service.events;

import org.springframework.context.ApplicationEvent;

import com.springmvc.elvisHamburgueseria.models.Mesa;

public class MesaEvent extends ApplicationEvent
{
    private static final long serialVersionUID = 1L;
     
    private String eventType;
    private Mesa mesa;
     
    //Constructor's first parameter must be source
    public MesaEvent(Object source, String eventType, Mesa mesa) 
    {
        //Calling this super class constructor is necessary
        super(source);
        this.eventType = eventType;
        this.mesa = mesa;
    }
 
    public String getEventType() {
        return eventType;
    }
 
    public Mesa getMesa() {
        return mesa;
    }
}
