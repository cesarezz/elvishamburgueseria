package com.springmvc.elvisHamburgueseria.service;

import com.springmvc.elvisHamburgueseria.models.Alimentos_Pedido;
import com.springmvc.elvisHamburgueseria.models.Pedido;

import java.util.List;

public interface PedidoService {
	
	public Integer insertMesa(String idPedido, String idMesa);
	
	public Pedido insert();
	
	public void cambiarPagado(Integer id);
	
	public Pedido pedidoPorId(Integer id);
	
	public Integer insertTiempoPrecio(Integer id, Integer tiempo, Double precio);
	
	public List<Pedido> todosPedidos();
	
	public Integer delete(Pedido pedido);

}
