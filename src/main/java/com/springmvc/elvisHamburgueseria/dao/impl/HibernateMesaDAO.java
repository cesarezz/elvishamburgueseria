package com.springmvc.elvisHamburgueseria.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

import com.springmvc.elvisHamburgueseria.dao.MesaDAO;
import com.springmvc.elvisHamburgueseria.models.Mesa;

public class HibernateMesaDAO implements MesaDAO {
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * gives hibernate3 Session
	 * 
	 * @return current hibernate Session
	 */
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	@Transactional
	public int insert(Mesa mesa) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		int id = (Integer) sessionFactory.getCurrentSession().save(mesa);
		mesa.setId(id);
		
		tx.commit();

		return id;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Mesa selectById(Integer id) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		Mesa mesa = (Mesa) sessionFactory.getCurrentSession().get(Mesa.class, id);

		tx.commit();
		
		return mesa;
	}
	

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Mesa> selectAll() {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		List<Mesa> mesas = null;
		mesas = sessionFactory.getCurrentSession().createQuery("from Mesa").list();
		
		tx.commit();
		
		return mesas;
	}
	

	@Transactional
	public void delete(Integer id) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		Mesa mesa = (Mesa) sessionFactory.getCurrentSession().load(Mesa.class, id);
		getSession().delete(mesa);
		
		tx.commit();
	}

	
	@Transactional
	public void update(Mesa mesa) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		sessionFactory.getCurrentSession().merge(mesa);
		
		tx.commit();
	}

	@Override
	public boolean exists(Integer id) {
		
		Boolean existe = false;
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		Mesa mesa = this.selectById(id);
		
		if (!mesa.equals(null)) existe = true;

		tx.commit();
		
		return existe;
	}
	
}