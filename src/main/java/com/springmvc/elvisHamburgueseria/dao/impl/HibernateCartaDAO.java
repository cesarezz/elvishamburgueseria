package com.springmvc.elvisHamburgueseria.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.springmvc.elvisHamburgueseria.dao.CartaDAO;
import com.springmvc.elvisHamburgueseria.models.Carta;

/**
 * Hibernate implementation for a UserDAO.
 * 
 * @author Eugenia Pérez Martínez.
 *
 */

public class HibernateCartaDAO implements CartaDAO {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * gives hibernate3 Session
	 * 
	 * @return current hibernate Session
	 */
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	@Transactional
	public int insert(Carta carta) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		int id = (Integer) sessionFactory.getCurrentSession().save(carta);
		carta.setId(id);
		
		tx.commit();

		return id;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Carta selectById(Integer id) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		Carta carta = (Carta) sessionFactory.getCurrentSession().get(Carta.class, id);

		tx.commit();
		
		return carta;
	}
	

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Carta> selectAll() {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		List<Carta> cartas = null;
		cartas = sessionFactory.getCurrentSession().createQuery("from Carta").list();
		
		tx.commit();
		
		return cartas;
	}
	

	@Transactional
	public void delete(Integer id) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		Carta carta = (Carta) sessionFactory.getCurrentSession().load(Carta.class, id);
		getSession().delete(carta);
	}

	
	@Transactional
	public void update(Carta carta) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		sessionFactory.getCurrentSession().merge(carta);
		
		tx.commit();
	}
	
}
