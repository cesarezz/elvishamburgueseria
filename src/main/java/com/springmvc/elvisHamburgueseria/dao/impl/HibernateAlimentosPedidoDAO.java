package com.springmvc.elvisHamburgueseria.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

import com.springmvc.elvisHamburgueseria.controllers.AlimentosPedidoController;
import com.springmvc.elvisHamburgueseria.dao.AlimentosPedidoDAO;
import com.springmvc.elvisHamburgueseria.models.Alimentos_Pedido;
import com.springmvc.elvisHamburgueseria.models.Alimentos_PedidoId;
import com.springmvc.elvisHamburgueseria.models.Carta;
import com.springmvc.elvisHamburgueseria.models.Johnny;
import com.springmvc.elvisHamburgueseria.models.Pedido;

public class HibernateAlimentosPedidoDAO implements AlimentosPedidoDAO {

	private SessionFactory sessionFactory;
	
	private Logger logger = Logger.getLogger(HibernateAlimentosPedidoDAO.class);

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * gives hibernate3 Session
	 * 
	 * @return current hibernate Session
	 */
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public HibernateAlimentosPedidoDAO(){
		
		BasicConfigurator.configure();
		  
		logger = Logger.getLogger("Logger de DAO Alimentos Pedido");
		  
		logger.setLevel(Level.INFO);
		
	}
	
	
	@Transactional
	public Alimentos_PedidoId insert(Alimentos_Pedido alimentos_pedido) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		// TODO Auto-generated method stub
		
		logger.info("Nuevo ALimentos_Pedido REST" + alimentos_pedido.toString());
		
		Alimentos_PedidoId id = (Alimentos_PedidoId) sessionFactory.getCurrentSession().save(alimentos_pedido);
		
		logger.info("Nuevo ALimentos_Pedido REST - Id Resultante" + id.toString());
		
		tx.commit();
		
		return id;
	}

	@Transactional(readOnly = true)
	public  List<Alimentos_Pedido> selectByPedido(Integer id) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		// TODO Auto-generated method stub
		
		Query query = sessionFactory.getCurrentSession().createQuery("from Alimentos_Pedido a where a.pedido_id = :idPedido");
		
		query.setParameter("idPedido", id);
		
		List<Alimentos_Pedido> alimentos_pedido = query.list();
		
		tx.commit();
		return alimentos_pedido;
	}
	
	@Transactional(readOnly = true)
	public Alimentos_Pedido selectByPedidoCarta(Integer idPedido, Integer idCarta) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		// TODO Auto-generated method stub
		
		/*Query query = sessionFactory.getCurrentSession().createQuery("from alimentos_pedido a where a.pedido_id = :idPedido and a.carta_id = :idCarta");
		
		query.setParameter("idPedido", idPedido);
		query.setParameter("idCarta", idCarta);
		
		List<Alimentos_Pedido> alimentos_pedido = query.list();
		
		Alimentos_Pedido alimento_pedido = alimentos_pedido.get(0);*/

		logger.info("Pedido Int" + idPedido.toString());
		
		logger.info("Carta Int" + idCarta.toString());		  
		
		Alimentos_PedidoId id = new Alimentos_PedidoId(idPedido, idCarta);
		
		logger.info("Primary key" + id.toString());	
		
		Alimentos_Pedido alimento_pedido = (Alimentos_Pedido) sessionFactory.getCurrentSession().get(Alimentos_Pedido.class, id);
		
		logger.info("Entidad Alimentos_Pedido" + alimento_pedido.toString());	
		
		tx.commit();
		
		return alimento_pedido;
	}

	@Transactional
	public Integer delete(Integer idPedido) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		Query query = sessionFactory.getCurrentSession().createQuery("from Alimentos_Pedido a where a.pedido_id = :idPedido");
		
		query.setParameter("idPedido", idPedido);
		
		List<Alimentos_Pedido> alimentos_pedido = query.list();
		
		
		for (Alimentos_Pedido alimentos_Pedido2 : alimentos_pedido) {
			
			sessionFactory.getCurrentSession().delete(alimentos_Pedido2);
			
		}
		
		tx.commit();
		
		return idPedido;
		
	}
	
	@Transactional
	public Alimentos_PedidoId deleteAlimentos(Alimentos_Pedido alimentos_pedido) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		sessionFactory.getCurrentSession().delete(alimentos_pedido);
		
		tx.commit();
		
		Alimentos_PedidoId alimentos_PedidoId = new Alimentos_PedidoId(alimentos_pedido.getPedido_id(),alimentos_pedido.getCarta_id());
		
		return alimentos_PedidoId;
		
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Alimentos_Pedido> getAll() {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		List<Alimentos_Pedido> alimentos_pedidos = null;
		alimentos_pedidos = sessionFactory.getCurrentSession().createQuery("from Alimentos_Pedido").list();
		
		tx.commit();
		
		return alimentos_pedidos;
	}

}
