package com.springmvc.elvisHamburgueseria.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

import com.springmvc.elvisHamburgueseria.dao.PedidoDAO;
import com.springmvc.elvisHamburgueseria.models.Mesa;
import com.springmvc.elvisHamburgueseria.models.Pedido;

public class HibernatePedidoDAO implements PedidoDAO{

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * gives hibernate3 Session
	 * 
	 * @return current hibernate Session
	 */
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	@Transactional
	public Integer insertMesa(Integer idPedidoInt,Integer idMesaInt) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		Pedido pedido = (Pedido) sessionFactory.getCurrentSession().get(Pedido.class, idPedidoInt);
		
		pedido.setPagado(false);
		
		Mesa mesa = (Mesa) sessionFactory.getCurrentSession().get(Mesa.class, idMesaInt);
		
		pedido.setMesa(mesa);
		
		sessionFactory.getCurrentSession().save(pedido);

		tx.commit();
		
		return idMesaInt;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Pedido selectById(Integer id) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		Pedido pedido = (Pedido) sessionFactory.getCurrentSession().get(Pedido.class, id);

		tx.commit();
		
		return pedido;
	}
	

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Pedido> selectAll() {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		List<Pedido> pedidos = null;
		pedidos = sessionFactory.getCurrentSession().createQuery("from Pedido").list();
		
		tx.commit();
		
		return pedidos;
	}
	

	@Transactional
	public Integer delete(Pedido pedido) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		sessionFactory.getCurrentSession().delete(pedido);
		
		tx.commit();
		
		return pedido.getId();
	}

	
	@Transactional
	public void update(Pedido pedido) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		sessionFactory.getCurrentSession().merge(pedido);
		
		tx.commit();
	}

	@Override
	public Pedido insert(Pedido pedido) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		// TODO Auto-generated method stub
		
		sessionFactory.getCurrentSession().save(pedido);
		
		tx.commit();
		
		return pedido;
	}

	@Override
	public Integer insertTiempoPrecio(Integer id, Integer tiempo, Double precio) {

		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		Pedido pedido = (Pedido) sessionFactory.getCurrentSession().get(Pedido.class, id);
		
		pedido.setTiempo(tiempo);
		
		pedido.setPrecio(precio);
		
		sessionFactory.getCurrentSession().update(pedido);
		
		tx.commit();
		
		return id;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Pedido> getPedidos() {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		List<Pedido> pedidos = null;
		pedidos = sessionFactory.getCurrentSession().createQuery("from Pedido p where p.pagado = false").list();
		
		tx.commit();
		return pedidos;
	}

	
}
