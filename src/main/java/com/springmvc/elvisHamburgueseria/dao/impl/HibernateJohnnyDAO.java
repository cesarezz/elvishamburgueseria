package com.springmvc.elvisHamburgueseria.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

import com.springmvc.elvisHamburgueseria.dao.JohnnyDAO;
import com.springmvc.elvisHamburgueseria.models.Johnny;
import com.springmvc.elvisHamburgueseria.models.Pedido;

public class HibernateJohnnyDAO implements JohnnyDAO{

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * gives hibernate3 Session
	 * 
	 * @return current hibernate Session
	 */
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Johnny selectByNombre(String nombre) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		// TODO Auto-generated method stub
		List<Johnny> johnnys = null;

		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Johnny j where j.nombre LIKE :name");
		query.setParameter("name", "%" + nombre + "%");
		johnnys = query.list();
		
		Johnny johnny = (Johnny) johnnys.get(0);
		
		tx.commit();
		
		return johnny;
	}

	@Override
	public void update(Johnny johnny) {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		sessionFactory.getCurrentSession().merge(johnny);
		
		tx.commit();	
	}

	@Override
	public Johnny getEstado() {
		
		Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
		
		Johnny johnny = (Johnny) sessionFactory.getCurrentSession().get(Johnny.class, "Johnny");
		
		tx.commit();	
		
		return johnny;
	}

}
