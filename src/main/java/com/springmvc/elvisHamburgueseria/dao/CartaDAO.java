package com.springmvc.elvisHamburgueseria.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import com.springmvc.elvisHamburgueseria.models.Carta;

import org.springframework.transaction.annotation.Transactional;


	//public class CartaDao extends GenericDAO<Carta> {

		/**
		 * Selects all users types by login
		 * 
		 * @return List of users
		 */
		
		/*@Transactional
		public Carta selectById(Integer id) {

			List<Carta> alimentos = null;

			Query query = getSession().createQuery(
					"from Carta c where c.id = :id");
			query.setParameter("id", id );
			alimentos = query.list();
			
			Carta alimento = alimentos.get(0);

			return alimento;
		}*/

	//}

public interface CartaDAO {

	/**
	 * This is the method to be used to create a record in the Student table.
	 * 
	 * @param user
	 *            user to be saved.
	 * 
	 * @return the ID of the saved user.
	 */
	public int insert(Carta carta);

	/**
	 * This is the method to be used to list down a record from the Student
	 * table corresponding to a passed student id.
	 * 
	 * @param id
	 * 
	 * @return the user found
	 */
	public Carta selectById(Integer id);

	/**
	 * This is the method to be used to list down all the records from the
	 * Student table.
	 * 
	 * @return the list of users.
	 */
	public List<Carta> selectAll();

	/**
	 * This is the method to be used to delete a record from the Student table
	 * corresponding to a passed student id.
	 */
	public void delete(Integer id);

	/**
	 * This is the method to be used to update a record into the Student table.
	 */
	public void update(Carta carta);
}

