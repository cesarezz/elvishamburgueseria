package com.springmvc.elvisHamburgueseria.dao;

import java.util.List;

import com.springmvc.elvisHamburgueseria.models.Johnny;

public interface JohnnyDAO {

	/**
	 * This is the method to be used to list down a record from the Student
	 * table corresponding to a passed student id.
	 * 
	 * @param id
	 * 
	 * @return the user found
	 */
	public Johnny selectByNombre(String nombre);

	/**
	 * This is the method to be used to update a record into the Student table.
	 */
	public void update(Johnny johnny);
	
	public Johnny getEstado();

}
