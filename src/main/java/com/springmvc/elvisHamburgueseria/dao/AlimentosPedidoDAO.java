package com.springmvc.elvisHamburgueseria.dao;

import java.util.List;
import java.util.ArrayList;

import org.hibernate.Query;

import com.springmvc.elvisHamburgueseria.dao.GenericDAO;

import com.springmvc.elvisHamburgueseria.models.Alimentos_Pedido;
import com.springmvc.elvisHamburgueseria.models.Alimentos_PedidoId;

import org.springframework.transaction.annotation.Transactional;

public interface AlimentosPedidoDAO {

		/**
		 * This is the method to be used to create a record in the Student table.
		 * 
		 * @param user
		 *            user to be saved.
		 * 
		 * @return the ID of the saved user.
		 */
		public Alimentos_PedidoId insert(Alimentos_Pedido alimentos_pedido);

		/**
		 * This is the method to be used to list down a record from the Student
		 * table corresponding to a passed student id.
		 * 
		 * @param id
		 * 
		 * @return the user found
		 */
		public Alimentos_Pedido selectByPedidoCarta(Integer idPedido, Integer idCarta);
		
		/**
		 * This is the method to be used to list down a record from the Student
		 * table corresponding to a passed student id.
		 * 
		 * @param id
		 * 
		 * @return the user found
		 */
		public List<Alimentos_Pedido> selectByPedido(Integer id);

		/**
		 * This is the method to be used to delete a record from the Student table
		 * corresponding to a passed student id.
		 */
		public Integer delete(Integer idPedido);
		
		public List<Alimentos_Pedido> getAll();
		
		public Alimentos_PedidoId deleteAlimentos(Alimentos_Pedido alimentos_pedido);

}
