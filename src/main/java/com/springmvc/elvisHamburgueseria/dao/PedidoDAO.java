package com.springmvc.elvisHamburgueseria.dao;

import java.util.List;

import org.hibernate.Query;

import com.springmvc.elvisHamburgueseria.dao.GenericDAO;
import com.springmvc.elvisHamburgueseria.models.Carta;
import com.springmvc.elvisHamburgueseria.models.Pedido;

import org.springframework.transaction.annotation.Transactional;
public interface PedidoDAO {

	/**
	 * This is the method to be used to create a record in the Student table.
	 * 
	 * @param user
	 *            user to be saved.
	 * 
	 * @return the ID of the saved user.
	 */
	public Integer insertMesa(Integer idPedidoInt,Integer idMesaInt);	
	
	/**
	 * This is the method to be used to create a record in the Student table.
	 * 
	 * @param user
	 *            user to be saved.
	 * 
	 * @return the ID of the saved user.
	 */
	public Pedido insert(Pedido pedido);

	/**
	 * This is the method to be used to list down a record from the Student
	 * table corresponding to a passed student id.
	 * 
	 * @param id
	 * 
	 * @return the user found
	 */
	public Pedido selectById(Integer id);

	/**
	 * This is the method to be used to list down all the records from the
	 * Student table.
	 * 
	 * @return the list of users.
	 */
	public List<Pedido> selectAll();

	/**
	 * This is the method to be used to delete a record from the Student table
	 * corresponding to a passed student id.
	 */
	public Integer delete(Pedido pedido);

	/**
	 * This is the method to be used to update a record into the Student table.
	 */
	public void update(Pedido pedido);
	
	public Integer insertTiempoPrecio(Integer id, Integer tiempo, Double precio);
	
	public List<Pedido> getPedidos();
}