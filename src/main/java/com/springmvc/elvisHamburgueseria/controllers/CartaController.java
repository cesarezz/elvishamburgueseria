package com.springmvc.elvisHamburgueseria.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.springmvc.elvisHamburgueseria.dao.CartaDAO;
import com.springmvc.elvisHamburgueseria.models.Carta;
import com.springmvc.elvisHamburgueseria.service.CartaService;
import com.springmvc.elvisHamburgueseria.service.MesaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/carta")
public class CartaController {
  
  @Autowired
  private CartaService cartaServiceImpl;
  
  private Logger logger = Logger.getLogger(CartaController.class);

  @RequestMapping(method = RequestMethod.GET, value = { "/" })
  public List<Carta> showAlimentos() {

    //model.addAttribute("clientes", personRepository.findAll());
    //return "clientes";
	  return cartaServiceImpl.showAlimentos();
  }
  
  @RequestMapping(method = RequestMethod.GET, value = { "/consulta/{id}"})
  public Carta showAlimentoById( @PathVariable(value = "id") String id) {
	  Integer idInt= new Integer(id);
	  return (Carta) cartaServiceImpl.showAlimentoById(idInt);
  }
  
}
