package com.springmvc.elvisHamburgueseria.controllers;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.springmvc.elvisHamburgueseria.dao.MesaDAO;
import com.springmvc.elvisHamburgueseria.dao.PedidoDAO;
import com.springmvc.elvisHamburgueseria.models.Alimentos_Pedido;
import com.springmvc.elvisHamburgueseria.models.Carta;
import com.springmvc.elvisHamburgueseria.models.Mesa;
import com.springmvc.elvisHamburgueseria.models.Pedido;
import com.springmvc.elvisHamburgueseria.service.PedidoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/pedido")
public class PedidoController {
	
	  /*@Autowired 
	  private PedidoDAO pedidoDAO;*/
	  
	  @Autowired
	  private PedidoService pedidoServiceImpl;
	  
	  private Logger logger;
	
	  public PedidoController(){
		  
		  BasicConfigurator.configure();
		  
		  logger = Logger.getLogger("Logger de Pedido");
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/{idPedido}/mesa/{idMesa}" })
	  public Integer insertMesa(@PathVariable(value = "idPedido") String idPedido, @PathVariable(value = "idMesa") String idMesa) {
		
		  return pedidoServiceImpl.insertMesa(idPedido, idMesa);
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/{id}/tiempo/{tiempo}/precio/{precio:.+}" })
	  public void insertTiempoPrecioPedido(@PathVariable(value = "id") String id,@PathVariable(value = "tiempo") String tiempo , @PathVariable(value = "precio") String precio) {
		
		  Integer idInt= new Integer(id);
		  
		  Integer tiempoInt= new Integer(tiempo);
		  
		  Double precioDouble = Double.parseDouble(precio);
		  
		  pedidoServiceImpl.insertTiempoPrecio(idInt, tiempoInt, precioDouble);
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/new" })
	  public Pedido insert() {
		
		  return pedidoServiceImpl.insert();
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/pagado/{id}" })
	  public void cambiarPagado(@PathVariable(value = "id") String id) {
		  
		  Integer idInt= new Integer(id);
		  pedidoServiceImpl.cambiarPagado(idInt);
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/{id}" })
	  public Pedido pedidoPorId(@PathVariable(value = "id") String id) {
		  
		  Integer idInt= new Integer(id);
		  return pedidoServiceImpl.pedidoPorId(idInt);
		    
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/all" })
	  public List<Pedido> todosPedidos() {
		  
		  return pedidoServiceImpl.todosPedidos();
		    
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/eliminar/all" })
	  public void eliminarAlimentosPedidos(){
		  
		 List<Pedido> pedidos = pedidoServiceImpl.todosPedidos();
		  
		 for (Pedido p: pedidos){
			 pedidoServiceImpl.delete(p);	  
		 }
		  
	  }

}
