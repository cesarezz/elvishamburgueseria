package com.springmvc.elvisHamburgueseria.controllers;

import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springmvc.elvisHamburgueseria.models.Alimentos_Pedido;
import com.springmvc.elvisHamburgueseria.models.Johnny;
import com.springmvc.elvisHamburgueseria.service.AlimentosPedidoService;
import com.springmvc.elvisHamburgueseria.service.JohnnyService;


@RestController
@RequestMapping("/johnny")
public class JohnnyController {
	
	  @Autowired
	  private JohnnyService johnnyServiceImpl;
	  
	  private Logger logger = Logger.getLogger(JohnnyController.class);
	  
	  public JohnnyController(){
		  
		  BasicConfigurator.configure();
		  
		  logger = Logger.getLogger("Logger de Johnny");
		  
		  logger.setLevel(Level.INFO);
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/estado" })
	  public Johnny getEstado(){
		  
		  return johnnyServiceImpl.getEstado();
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/iniciar" })
	  public void inicializaJohnny(){
		  
		  johnnyServiceImpl.inicializaJohnny();
		  
	  }

}
