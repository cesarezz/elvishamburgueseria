package com.springmvc.elvisHamburgueseria.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.springmvc.elvisHamburgueseria.dao.MesaDAO;
import com.springmvc.elvisHamburgueseria.models.Carta;
import com.springmvc.elvisHamburgueseria.models.Mesa;
import com.springmvc.elvisHamburgueseria.service.MesaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/mesa")
public class MesaController {
	
	  /*@Autowired 
	  private MesaDAO mesaDAO;*/
	  
	  /*@Autowired
	  private MesaService mesaService;*/
	  
	  @Autowired
	  private MesaService mesaServiceImpl;
	  
	  private Logger logger = Logger.getLogger(CartaController.class);

	  @RequestMapping(method = RequestMethod.GET, value = { "/{id}/estado/{estado}" })
	  public void cambiaEstado(@PathVariable String id, @PathVariable String estado) {		  	  
		  
		  Mesa mesa = this.showMesaById(id);
		  
		  mesa.setEstado(estado);	  
		  
		  mesaServiceImpl.cambiaEstado(mesa);  
		  
		  /*mesa.setEstado(estado);	
		   *mesaDAO.update(mesa);		  */
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/consulta/{id}"})
	  public Mesa showMesaById(@PathVariable(value = "id") String id) {
		  
		  Integer idInt= new Integer(id);
		
		  //return mesaDAO.selectById(idInt);
		  return mesaServiceImpl.showMesaById(idInt);
	  }

}
