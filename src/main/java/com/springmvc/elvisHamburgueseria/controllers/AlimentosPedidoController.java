package com.springmvc.elvisHamburgueseria.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.springmvc.elvisHamburgueseria.models.AlimentosPedidoWrapper;
import com.springmvc.elvisHamburgueseria.models.Alimentos_Pedido;
import com.springmvc.elvisHamburgueseria.models.Alimentos_PedidoId;
import com.springmvc.elvisHamburgueseria.models.Carta;
import com.springmvc.elvisHamburgueseria.service.AlimentosPedidoService;


@RestController
@RequestMapping("/alimentosPedido")
public class AlimentosPedidoController {	
	
	  @Autowired
	  private AlimentosPedidoService alimentosPedidoServiceImpl;
	  
	  private Logger logger = Logger.getLogger(AlimentosPedidoController.class);
	  
	  public AlimentosPedidoController(){
		  
		  BasicConfigurator.configure();
		  
		  logger = Logger.getLogger("Logger de Alimentos Pedido Service");
		  
		  logger.setLevel(Level.INFO);
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.POST, value = { "/new" })
	  public ArrayList<Alimentos_PedidoId> createAlimentosPedido(@RequestBody AlimentosPedidoWrapper wrapper) {
		  
		  //logger.info("Alimentos Pedido" + alimentos_pedido.toString());
		
		  ArrayList<Alimentos_PedidoId> alimentos_PedidoId = new  ArrayList<Alimentos_PedidoId>();
		  
		  Alimentos_PedidoId id = null;
		  
		  //logger.info("Pedido Int" + wrapper.getAlimentosPedidos().toString());
		  
		  for (Alimentos_Pedido ap: wrapper.getAlimentosPedidos()){
			  id = alimentosPedidoServiceImpl.insert(ap);
			  alimentos_PedidoId.add(id);
		  }
		  
		  /*if (!bindingResult.hasErrors()) {
			  alimentos_PedidoId = alimentosPedidoServiceImpl.insert(alimentos_pedido);
		  } else {
			  logger.info(bindingResult.getAllErrors().toString());
			  
		  }*/
		  
		 return alimentos_PedidoId;
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/pedido/{idPedido}/carta/{idCarta}" })
	  public Alimentos_Pedido buscarPorPedidoCarta(@PathVariable(value = "idPedido") String idPedido , @PathVariable(value = "idCarta") String idCarta){
		
		  Integer pedidoInt = new Integer(idPedido);
				  
		  Integer cartaInt = new Integer(idCarta);
		  
		  logger.info("Pedido Int" + idPedido);
		  
		  logger.info("Carta Int" + idCarta);
		  
		  return alimentosPedidoServiceImpl.selectByPedidoCarta(pedidoInt, cartaInt);	  
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/pedido/{idPedido}" })
	  public List<Alimentos_Pedido> buscarPorPedido(@PathVariable(value = "idPedido") String idPedido){
		
		  Integer pedidoInt = new Integer(idPedido);
		  
		  logger.info("Pedido Int" + pedidoInt.toString());
		  
		  return alimentosPedidoServiceImpl.selectByPedido(pedidoInt);	  
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/pedido/{idPedido}/eliminar" })
	  public void eliminarAlimentosPorPedido(@PathVariable(value = "idPedido") String idPedido){
		
		  Integer pedidoInt = new Integer(idPedido);
		  
		  logger.info("Pedido Int" + pedidoInt.toString());
		  
		  alimentosPedidoServiceImpl.delete(pedidoInt);	  
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/all" })
	  public List<Alimentos_Pedido> getAllPedidos(){
		  
		  return alimentosPedidoServiceImpl.getAll();
		  
	  }
	  
	  @RequestMapping(method = RequestMethod.GET, value = { "/eliminar/all" })
	  public void eliminarAlimentosPedidos(){
		  
		 List<Alimentos_Pedido> alimentos_pedidos = alimentosPedidoServiceImpl.getAll();
		  
		  for (Alimentos_Pedido ap: alimentos_pedidos){
			  alimentosPedidoServiceImpl.deleteAlimentos(ap);	  
		  }
		  
	  }

}
