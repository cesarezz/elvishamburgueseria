# Elvis Presley’s Burger #

Dicha  hamburguesería  está  situada  en  el  centro  de  Madrid  y  ofrece  una 
escueta, pero selecta, carta de hamburguesas, refrescos y postres (al final del 
documento está la carta ofertada).

### Organización del comedor ###

Cuando  un  grupo  de  clientes  llega  al  Elvis  Presley’s  Burger,  se  sitúa 
en  una  fila  y  espera  a  que  una  mesa  queda  libre.  Tan  pronto  como 
queda  una  libre,  el  primer  grupo  de  la  fila  la  ocupa  y  nuestro 
camarero Johnny va a tomar nota. Hecho esto, Johnny pasa la nota a 
la  cocina  del  local  y  esta  le  avisa  a  medida  que  va  preparando  la 
comida.  Cuando  los  clientes  han  terminado  de  degustar  los 
maravillosos  platos,  piden  a  Johnny  la  cuenta,  este  la  lleva,  los 
clientes pagan, liberan la mesa y Johnny la limpia.
Cada  mesa  puede  estar  libre,  comiendo  o  esperando  la  cuenta 
(suponemos  que  los  clientes  abonan  la  cuenta  nada  mas  recibirla, 
liberan la mesa y Johnny la recoge).

### Pedidos ###

La  carta  de  la  hamburguesería  es  escasa  pero  suficientemente 
variada.  Los  pedidos  que  hacen  los  clientes  pueden  incluir  cualquier 
elemento  del  menú  (por  el  momento  obviaremos  las  hamburguesas 
My  Way).  A  parte  de  lo  indicado  en  la  carta,  se  anuncian  mediante 
letreros un menú:
American  Trilogy:  hamburguesa  Suspicious  Minds,  Cheese  Fries  y 
Coca-Cola por sólo 11€. 
El  camarero  una  vez  tomada  la  nota  la  lleva  a  la  cocina  para  que 
vayan preparándolo todo.
En  el  caso  de  la  hamburguesa  My  Way,  el  cliente  puede  añadir 
ingredientes  a  una  base  de  manera  ilimitada  (puede  poner  doble  o 
triple  de  queso,…).  Luego  debemos  almacenar  de  manera  correcta 
todos los ingredientes extra que el cliente desea. 
A petición de Johnny se ha incluido la opción "Yo también" dentro del 
sistema.  Básicamente  consiste  en  que  se  pueda  duplicar  fácilmente 
algo  que  haya  pedido  un  usuario  (esto  es  especialmente  importante 
en  el  caso  de  la  hamburguesa  "My  Way"  en  la  que  los  usuarios 
escogen entre una amplia lista de complementos). Parecido pasa con 
los refrescos con refill.

### Carta ###

#### Hamburguesas ####
Suspicious  Mind: maravillos hamburguesa, también e ¼ de libra, con 
cebolla a la plancha, bacon y queso por sólo 5,5€. 
My Way: incredible hamburguesa de ¼ de libra de primera calidad con 
lechuga  y  tomate  e  ingredientes  a  escoger  entre:  relish  (salsa  de 
pepinillo dulce picado), bacon y queso cheddar por 4€ la base y 0,5€ 
cada ingrediente. 
#### Postres ####
New  York  cheese  cake:  la  original  a  la  vuelta  de  la  esquina,  por 
4,95€. 
#### Refrescos ####
Coca-Cola o Seven-Up: por 2€ el refresco. ¡Por 5€ con refill! 
#### Side Orders ####
Chesse Fries: con queso cheddar y bacon por sólo 3€. 

## Entorno tecnológico ##

* Java
* SpringMVC
* Hibernate
* JUnit
* Mockito
